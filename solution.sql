/*
        Ephraim Khirt O. Perez
        
     A.List books Authored by Marjorie Green
      BU1032  - "The Busy Executives Database Guide"
      BU2075  - "You Can Combat Computer Stress"

     B.List the books Authored by Michael O'Leary
      BU1111 - "Cooking with Computers"
     
     C.Write the author/s of "The Busy Executives Database Guide"
      213-46-8915 - "Marjorie Green"
      409-56-7008 - "Abraham Bennet"

     D.Identify the publisher of "But is it User Friendly"
      1389 - "Algodata Infosystem"

     E.List the books published by Algodata Infosystem
      PC1035 - "The Busy Executives Database Guide"
      BU1032 - "Cooking with Computers"
      BU1111 - "Straight Talk About Computers"
      BU7832 - "But is it User Friendly"
      PC8888 - "Secrets of Silicon Valley"
      PC9999 - "Net Etiquette"
*/

 --Create a database for a blog using SQL syntax

CREATE DATABASE blog_db;

USE blog_db;

-- For Authors
CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_create DATETIME NOT NULL,
PRIMARY KEY(id)
);

-- For Posts
CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_author_id
    FOREIGN KEY(author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- For Posts_Comments
CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id
    FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_user_id
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- For Likes
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id2
    FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_user_id2
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
